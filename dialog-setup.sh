#!/bin/bash
# selfhost 版 reticulum セットアップスクリプト

echo "selfhost 版 Mozilla Hubs セットアップ"

echo "dialog をセットアップします"
cd dialog
npm install
mkdir certs
cd certs
rm *.pem
ln -s $HOME/fullchain.pem fullchain.pem
ln -s $HOME/privkey.pem privkey.pem
ln -s $HOME/mozilla-hubs-selfhost/reticulum/priv/perms.pub.pem .
cd ..
