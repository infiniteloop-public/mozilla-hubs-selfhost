##  【メタバースチャレンジ2022】はじめてのアバター作成
### 概要
本資料では簡易的なオリジナルアバターを作成してHubsへアップロードして使用するまでの手順について解説します。  
基本的にアバターメーカーを使用して手軽に用意する方法について記述していますが、追加でアバターをフルスクラッチする際の技術的情報についても記載してあります。

Hubsでは他のVRSNS同様、各個人がアバターとして3Dモデルデータをアップロードして仮想空間内で使用することができます。
Meta QuestやHTC ViveなどのHMDを使用してHubsにログインすることでアバターに自身の手や頭の位置が反映させることができます。これによりテキストベースのSNSと違いジェスチャーなどの多彩な表現を用いたコミュニケーションを取ることができます。

本家[Hubs moz://a](https://hubs.mozilla.com/)では初期アバターとして数十種類のアバターが用意されており自分のアバターをアップロードせずともすぐに始められるようになっています。

![image](images/avatar/02.png)

### アバターの作成方法
Hubsで使用するアバターを作成するには、お手軽にオリジナルアバターを用意したい人向けのアバターメーカーサービスでプリセットや画像を選び生成する方法と[Blender](https://www.blender.org/)や[Maya](https://www.autodesk.co.jp/products/maya/)などのDCC（Digital Content Creation）ツールで一からフルスクラッチ、または既存の[VRM](https://vrm.dev/)などのアバターフォーマットのデータが手元にある場合、それらをHubs向けに変換する方法の二通りがあります。どちらも最終的に`.gbl`という拡張子の付いたglTF（GL Transmission Format）というフォーマットで出力され、そのファイルをHubsへアップロードすることでアバターとして使用可能となります。

アバターメーカーには下記のようなものがあります
- [Quilt](http://tryquilt.io/)
- [IEEE VR 2020 | Avatar Customizer.](https://www.qt-mkr.com/)
- [Hackweek Avatar Maker](https://mozilla.github.io/hackweek-avatar-maker/)

### 実際にアバターを作成しアップロードしてみる
今回はmozillaが保守をしているアバターメーカーの[Hackweek Avatar Maker](https://mozilla.github.io/hackweek-avatar-maker/)を使用してオリジナルのアバターを作成し、アップロードしてみましょう。

> どのような実装になっているか興味がある方は[こちらのGitHubリポジトリ](https://github.com/mozilla/hackweek-avatar-maker)を見てみましょう。

ログインなどは不要でブラウザ上からすぐにアバターの作成を始めることができます。  
画面左の一覧からアバターの見た目を操作することができます。色やアクセサリーなども追加で指定できるようです。  
アバターのカスタマイズが完了したら最後に画面右下の`Export avatar`を選択することで作成したアバターのデータをダウンロードすることができます。

![image](images/avatar/06.png)

`custom_avatar.glb`というファイルがダウンロードされます。[3Dビュアー](https://apps.microsoft.com/store/detail/3d-viewer/9NBLGGH42THS)がインストールされている環境の場合glbファイルを開き見た目を確認することができます。

![image](images/avatar/07.png)

### アバターのアップロードと変更方法
画面右下のメニューから`Change Name & Avatar`を選択しアバター変更画面から手元にある3Dモデルデータを新規アバターとしてアップロードしたりアップロード済みライブラリからアバターを選択することができます。  

|![image](images/avatar/01.png)|![image](images/avatar/03.png)|
|-|-|
|||

自身のアカウントが所有しているアセット一覧が表示されます。新規にアバターをアップロードする際は`アバター作成`ボタンを押します。
`Custom GLB`を選択するとローカルのファイルを選択するウインドウが表示されます。先程ダウンロードした`custom_avatar.glb`を選択しましょう。  
正常にプレビューが表示されていればアップロードは成功です。アバターの名前を設定し`Save`を選択しましょう。

※`Advanced`から細かな設定などができますが今回は特に設定の変更は不要です。

|![image](images/avatar/08.png)|![image](images/avatar/09.png)|
|-|-|
|||

ライブラリーにアップロードしたアバターが増えていることが確認できるはずです。早速選択してみましょう。  
最初の画面に戻り選択したアバターが表示されます。ついでに`Display Name`からルームでの表示名を設定できるので設定しておきましょう。
最後に`Accept`を選択すれば変更完了です。
![image](images/avatar/10.png)

特に画面に変化がないので正常に変更されたか心配になると思います。ルーム内で自身の姿を確認するにはカメラというアイテムを使用すると良いです。  
`Place` -> `Camera`から目の前にカメラを出現させることができます。初期で自身の方を向いてセルフィーのようになっているので自分の姿が確認できるはずです！

![image](images/avatar/11.png)

## アバターの技術的仕様
アバターをフルスクラッチで作成する際に参考となる情報をまとめておきます。  
Hubs公式ドキュメントはこちら [Advanced Avatar Customization ? Hubs by Mozilla](https://hubs.mozilla.com/docs/creators-advanced-avatar-customization.html)  
デフォルトのロボットのようなアバターがリファレンスとして[MozillaReality/hubs-avatar-pipelines](https://github.com/MozillaReality/hubs-avatar-pipelines)に公開されています。基本的にこちらで公開されている.blendファイルを開いて改変するのが手っ取り早いでしょう。

### オブジェクト・メッシュ
基本的に全てのオブジェクトは結合し1メッシュにします。オブジェクトのルートの名前は`AvatarRoot`、メッシュオブジェクトの名前は`Avatar`にするのが望ましいようです。

![image](images/avatar/04.png)

ボディーのメッシュに`MouthOpen`というシェープキーが存在する場合、Hubs内で発言する際にマイクの音量に応じてトリガーされます。発言時アバターの口を動かしたいときなどに使用すると良いでしょう。

### マテリアル
Hubsのアバターはリアル調の絵柄が出るBSDFでシェーディングされるようです。アニメキャラクターのようなToon調の表現は難しいかもしれません。

> 筆者の環境ではメッシュと同様にMaterialもメッシュに対して一つしかない状態ではないと正しく動作しないようでした。一つのメッシュに対して複数のマテリアルが割あたっている場合は一つに統合する必要があるかもしれません。

公式のリファレンスアバターのマテリアルノードはこのようになっていることからベースカラーだけでなくノーマルマップやスムースネス、メタリック、シェーダー加算による発光などをサポートしているようです。
![image](images/avatar/05.png)

上記のような複雑なノードではなく単純に色のみの指定だけで十分な場合は下記のようにベースカラーのみの指定でも問題なく表示されるようです。
![image](images/avatar/12.png)

### ボーン
ボーン構造や名前はHubsのコードにハードコートされているようなので下記の構造を厳守する必要があります。  
下記のボーンが存在しない、構造が異なる場合はエラーアバターとなり正常に表示されません。  

HeadボーンはHMDまたはマウス操作によって視点方向に追従して回転します。  
RightHand、LeftHandはHMDを使用してHubsにログインしている際にコントローラーに追従して動きます。HMD未使用の際はスケールがゼロとなり非表示になるようです
```css
AvatarRoot
└── Hips
    └── Spine
        ├── Neck
        │   └── Head
        │       ├── RightEye
        │       └── LeftEye
        ├── RightHand
        │   ├── RightHandIndex1
        │   │   └── RightHandIndex2
        │   │       └── RightHandIndex3
        │   ├── RightHandMiddle1
        │   │   └── RightHandMiddle2
        │   │       └── RightHandMiddle3
        │   ├── RightHandRing1
        │   │   └── RightHandRing2
        │   │       └── RightHandRing3
        │   ├── RightHandPinky1
        │   │   └── RightHandPinky2
        │   │       └── RightHandPinky3
        │   └── RightHandThumb1
        │       └── RightHandThumb2
        │           └── RightHandThumb3
        └── LeftHand
            ├── LeftHandIndex1
            │   └── LeftHandIndex2
            │       └── LeftHandIndex3
            ├── LeftHandMiddle1
            │   └── LeftHandMiddle2
            │       └── LeftHandMiddle3
            ├── LeftHandRing1
            │   └── LeftHandRing2
            │       └── LeftHandRing3
            ├── LeftHandPinky1
            │   └── LeftHandPinky2
            │       └── LeftHandPinky3
            └── LeftHandThumb1
                └── LeftHandThumb2
                    └── LeftHandThumb3
```


### テクスチャ
Hubsはモバイル端末でも動作する軽量プラットフォームのためアバターは軽量であることが望ましいです。そのためテクスチャの解像度は一辺1024px以下を強く推奨します。
また、正方形かつ一辺が2の累乗ピクセルでなければならないようです。

### アニメーション
瞬きや呼吸などの一定間隔で永続的に動作してほしい動きや、握りこぶしや指差しなどといった手の形を定義しHMD使用時にハンドジェスチャーなどを出すために使用します。  
アニメーションは基本的に省略可能で設定しなくても動作上問題ないです

下記の名前で手のボーンの形状を定義したアニメーションを作成することでコントローラーの操作で指定のアニメーションを再生させハンドジェスチャーをアバターに適応することができます。

|左手|右手|動作|
|-|-|-|
|allOpen_L|allOpen_R|手を開く|
|thumbDown_L|thumbDown_R|親指曲げ|
|indexDown_L|indexDown_R|人差し指曲げ|
|mrpDown_L|mrpDown_L|指差し|
|pinch_L|pinch_R|つまむ|
|thumbsUp_L|thumbsUp_R|親指上げ|
|allGrip_L|allGrip_R|握りこぶし|



