#!/bin/bash
# selfhost 版 reticulum セットアップスクリプト

echo "selfhost 版 Mozilla Hubs セットアップ"
source ../env.sh

echo "reticulum のコンフィグを変更します"
sed -e "s/SELFHOST/$SELFHOST/g" reticulum-dev.exs > reticulum-prod.exs
sed -i -e "s/MAILSERVER/$MAILSERVER/g" reticulum-prod.exs
sed -i -e "s/MAILPORT/$MAILPORT/g" reticulum-prod.exs
sed -i -e "s/MAILUSER/$MAILUSER/g" reticulum-prod.exs
sed -i -e "s/MAILPASS/$MAILPASS/g" reticulum-prod.exs

cat reticulum/priv/perms.priv.pem.line >> reticulum-prod.exs

cp -pf reticulum-prod.exs reticulum/config/prod.exs
