#!/bin/bash
# selfhost 版 hubs セットアップスクリプト
# 引数にテストディレクトリ (tmp/) を指定してテスト可能

echo "selfhost 版 Mozilla Hubs セットアップ"
source ../env.sh

echo "hubs のコンフィグを変更します"
sed -e "s/SELFHOST_ESCAPED/$SELFHOST_ESCAPED/g" hubs-webpack.config.js | sed -e "s/SELFHOST/$SELFHOST/g" > $1hubs/webpack.config.js

mkdir -p hubs/certs/ 
rm hubs/certs/*.pem
ln -s $HOME/fullchain.pem hubs/certs/cert.pem
ln -s $HOME/privkey.pem hubs/certs/key.pem

cd hubs
npm ci
