#!/bin/bash
# selfhost 版 spoke セットアップスクリプト

echo "selfhost 版 Mozilla Hubs セットアップ"
source ../env.sh

echo "spoke のコンフィグを変更します"
sed -e "s/SELFHOST/$SELFHOST/g" spoke-webpack.config.js > Spoke/webpack.config.js
sed -e "s/SELFHOST/$SELFHOST/g" spoke-run-local-reticulum.sh > Spoke/scripts/run-local-reticulum.sh

cd Spoke
npm install -g yarn
npm install -g cross-env
yarn install

mkdir certs
rm certs/*.pem
ln -s $HOME/fullchain.pem certs/cert.pem
ln -s $HOME/privkey.pem certs/key.pem
