# mozilla-hubs-selfhost

## 目次

- [Mozilla Hubs サーバーセットアップ](doc/hubs-server-setup.md)
- [【メタバースチャレンジ2022】はじめてのシーン作成](doc/beginning-createscene.md)
- [【メタバースチャレンジ2022】はじめてのアバター作成](doc/beginning-create-avatar.md)