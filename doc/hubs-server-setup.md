# Mozilla Hubs サーバーセットアップ

このリポジトリを使った Mozilla Hubs のセルフホスト版の設定方法について解説します。

（資料や画像に出てくるドメイン名、IP アドレスはあくまで例や架空のものに過ぎません）

### OS 側前提

Linux のセットアップについては以下を前提としています。	

* サーバーの必要スペック 4 CPU / 5GB メモリ / 40GB ディスク
* Debian 11 で SSH server 含めてインストールされていること。GUI デスクトップ等のパッケージは不要です。
* サーバーはインターネットからのアクセスが可能な public な IP アドレスを持っている
* そのアドレスで登録された DNS の FQDN が存在している
* インストール時設定したユーザーで ssh アクセスが可能
* sudo がインストールされていて設定ユーザーで root 権限を利用可能

### OS 設定 root ユーザーでの作業

システムの言語環境を英語にします

    localectl set-locale en_US.UTF-8

fs.inotify.max_user_watches が 50000 より小さい場合

    sysctl -a |grep fs.inotify.max_user_watches で 50000より小さい場合
    /etc/sysctl.conf に  fs.inotify.max_user_watches = 50000  を追加
    sysctl --system 

hubs ユーザーを追加

    adduser hubs
    usermod -aG sudo hubs

### Let's Encrypt を使った SSL サーバー証明書取得

(root を継続して使います）python version 3 の起動コマンドが python3 であるか、python なのか確認してください。（通常は python3）

    # python3 --version
    Python 3.9.2

pip3 または pip コマンドが入っているか、またその対象が前述の python と同じか確認します。

    # pip3 --version 
    pip 20.3.4 from /usr/lib/python3/dist-packages/pip (python 3.9)
    
(python 〜) の部分を確認してください。入っていない場合は python3-pip をインストールします。

    apt insatll python3-pip

certbot をインストールします

    pip3 install certbot

apache2 や nginx が動いていると port 80 で競合して動作しないため、もしインストールしてある場合は停止しておきます。（この後の hubs でも使いませんので apache2/nginx はアンインストールしても問題ないです）

次に certbot コマンドで SSL を取得します。

    certbot certonly --agree-tos --email 登録メールアドレス -d hubs.example.com
    
メールアドレスは Lets Encrypt に登録するメールアドレスで特に制限はありませんが、各チームに準備してあるメールアドレスを使うのが良いです

-d で指定するドメイン名はみなさんのサーバーのホスト名（ドメイン名付き）を記入します。

    # certbot certonly --agree-tos --email n-hatano@infiniteloop.co.jp -d ドメイン名
    Saving debug log to /var/log/letsencrypt/letsencrypt.log

    How would you like to authenticate with the ACME CA?
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    1: Spin up a temporary webserver (standalone)
    2: Place files in webroot directory (webroot)
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    Select the appropriate number [1-2] then [enter] (press 'c' to cancel): 1     ＜＝＝＝ 1 を選択してください＝＝＝

    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    Would you be willing, once your first certificate is successfully issued, to
    share your email address with the Electronic Frontier Foundation, a founding
    partner of the Let's Encrypt project and the non-profit organization that
    develops Certbot? We'd like to send you email about our work encrypting the web,
    EFF news, campaigns, and ways to support digital freedom.
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    (Y)es/(N)o: Y
    Account registered.
    Requesting a certificate for ドメイン名

成功すると `/etc/letsencrypt/live/ドメイン名/〜` に証明書と鍵が生成されます。

稀にそのドメイン名ではポリシーによって取得することが出来ません。というメッセージが出る場合があります。
このようなケースになった場合はメンターにご相談ください

    Error creating new order :: Cannot issue for "ec2-18-181-189-165.ap-northeast-1.compute.amazonaws.com": The ACME server refuses to issue a certificate for this domain name, because it is forbidden by policy

あるホストに関して、1日に certbot で取得を試みられるのは数回までと決まっています。
最初の1回目で失敗してもあわてて何度も再実行せず、どこが間違っていたかじっくり原因を調査しましょう。

### OS 設定、以下 hubs ユーザーでの作業

追加パッケージ

    sudo apt install coturn
    sudo apt install libssl-dev automake autoconf gcc make libncurses5-dev unzip

SSL 証明書をコピーする (Lets Encrypt のものが /etc/〜 に配置されている場合の例)

    sudo cp /etc/letsencrypt/live/hubs.example.com/fullchain.pem ~/fullchain.pem
    sudo cp /etc/letsencrypt/live/hubs.example.com/privkey.pem ~/privkey.pem
    sudo chown hubs. ~/*.pem

Docker セットアップ

    sudo apt-get update
    sudo apt-get install ca-certificates curl gnupg lsb-release
    sudo mkdir -p /etc/apt/keyrings
    
    curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg

    echo \
    "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
    $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

    sudo apt-get update
    sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin

asdf と node.js v12 インストール

    git clone https://github.com/asdf-vm/asdf.git ~/.asdf
    echo '. $HOME/.asdf/asdf.sh' >> ~/.bashrc
    echo '. $HOME/.asdf/completions/asdf.bash' >> ~/.bashrc
    exec $SHELL -l
    asdf plugin add nodejs
    asdf list all nodejs  # 一度リストしないと次の install nodejs が失敗するようです
    asdf install nodejs lts-erbium
    asdf global nodejs lts-erbium

セルフホスト向け Mozilla Hubs リポを clone する

    cd
    git clone --recursive https://gitlab.com/infiniteloop-public/mozilla-hubs-selfhost.git 
    cd mozilla-hubs-selfhost

PostgreSQL 起動

    sudo docker compose up -d

### 設定ファイル作成

自分のホスト名、メール送信に使う外部メールサービスのアカウント情報を記述したファイルを用意します

    cd
    env.sh ファイルの作成（以下例）

```
SELFHOST=hubs.example.com
SELFHOST_ESCAPED='hubs\\.example\\.com'
MAILSERVER=メール送信に使う SMTP メールサーバーのアドレスまたはホスト名
MAILPORT=メールサーバーのポート
MAILUSER=メールアカウント
MAILPASS='パスワード'
MYIP=このサーバーに外部からアクセスするときの IP    
```

注意: MAILPASS の値はシングルクォートで囲ってください

### admin サーバーセットアップと起動

admin は 管理画面を提供しているサーバーです。
通常使用時はアクセスすることはありませんが、起動していないと全体が正しく稼働しないため必要となります。

    cd ~/mozilla-hubs-selfhost
    ./admin-setup.sh （時間かかります）
    ./admin-run.sh

でセットアップし、起動します。

    > hubs-admin@0.0.1 local /home/hubs/hubs-selfhost/hubs/admin
    > webpack-dev-server --mode=development --env.local

    ℹ ｢wds｣: Project is running at https://hubs.example.com:8989/
    ℹ ｢wds｣: webpack output is served from https://hubs.example.com:8989/
    ℹ ｢wds｣: Content not from webpack is served from /home/hubs/hubs-selfhost/hubs/admin
    Browserslist: caniuse-lite is outdated. Please run next command `npm update`
    ℹ ｢wdm｣: wait until bundle finished: /
    ℹ ｢wdm｣: Hash: e2e5cf6c0b57e42ea016

    * * * 

ブラウザで https://hubs.example.com:8989/ （例です）にアクセスして以下の画面が表示され、端末では

![](images/admin.png)

    ℹ ｢wdm｣: Compiled successfully.

というメッセージが表示されたら admin は無事起動しています。この端末はそのまま止めずにおきます。

上手く起動しない場合は、hubs/admin ディレクトリで `npm ci` を実行してみてから、ディレクトリを戻って `hubs-run.sh` を実行

### hubs サーバーセットアップと起動

新しく別の ssh 端末を開いて作業します。

トップページの画面の材料となるデータを提供しているサーバーが hubs サーバーです。

    cd ~/mozilla-hubs-selfhost
    ./hubs-setup.sh （時間かかります）
    ./hubs-run.sh

無事起動されると以下のメッセージなります

    > hubs@0.0.1 local /home/hubs/hubs-selfhost/hubs
    > webpack-dev-server --mode=development --env.localDev

    (node:31654) [DEP0131] DeprecationWarning: The legacy HTTP parser is deprecated.
    ℹ ｢wds｣: Project is running at https://hubs.example.com:8080/webpack-dev-server/
    ℹ ｢wds｣: webpack output is served from https://hubs.example.com:8080/
    ℹ ｢wds｣: 404s will fallback to /index.html

    * * *

ここでブラウザで https://hubs.example.com:8080 （例です）にアクセスすると以下の画面が表示され、

![](images/hubs.png)

端末でも

    ℹ ｢wdm｣: Compiled successfully.

というメッセージが表示されたら hubs は無事起動しています。
この端末はそのままにして、別の ssh 端末を開いてサーバーにアクセスしてください。

上手く起動しない場合は、hubs ディレクトリで `npm ci` を実行してみてから `hubs-run.sh` を実行

### Spoke サーバーのセットアップと起動

シーン作成のためのアセット管理やシーンエディタのサーバーが Spoke です。

    cd ~/mozilla-hubs-selfhost
    ./spoke-setup.sh （時間かかります）
    ./spoke-run.sh

で以下のメッセージなります。

    yarn run v1.22.19
    $ cross-env NODE_ENV=development webpack-dev-server --mode development
    ℹ ｢wds｣: Project is running at https://hubs.example.com:9090/
    ℹ ｢wds｣: webpack output is served from https://hubs.example.com:9090/
    ℹ ｢wds｣: Content not from webpack is served from /home/hubs/hubs-selfhost/Spoke
    ℹ ｢wds｣: 404s will fallback to /index.html

    * * *
    ℹ ｢wdm｣: Compiled successfully.

ブラウザで https://hubs.example.com:9090 （例です）にアクセスし以下の画面が表示されれば成功です

![](images/spoke.png)

### Reticulum サーバーのセットアップと起動

Mozilla Hubs のサーバー本体となって音声以外のすべてを処理しているのが Reticulum です。

hubs ユーザーにて、Erlang/Elixir を asdf でセットアップします

    asdf plugin add erlang
    asdf install erlang 22.3.4.24
    asdf global erlang 22.3.4.24
    asdf plugin add elixir
    asdf install elixir 1.8.2-otp-22
    asdf global elixir 1.8.2-otp-22

次に、設定はそのままフレームワークのビルドを行います

    cd ~/mozilla-hubs-selfhost/reticulum

以下のコマンドを実行

    mix local.hex --force
    mix deps.get
    mix local.rebar --force
    mix ecto.create （時間かかります）
    mkdir -p storage/dev

SSL のサーバー証明書を配置します

    rm priv/server.*
    ln -s $HOME/fullchain.pem priv/server.crt
    ln -s $HOME/privkey.pem priv/server.key

Dialog との通信用の秘密鍵・公開鍵を生成します

    cd priv
    openssl genrsa 1024 > perms.priv.pem
    openssl rsa -pubout < perms.priv.pem > perms.pub.pem

    （表示によっては改行がされているかもしれないですが、以下は１行で実行してください）

    sed -z 's/\n/\\n/g' perms.priv.pem |sed -E 's/\\n$/\n/g' |sed -E 's/^/config :ret, Ret.PermsToken, perms_key: "/g' | sed -E 's/$/"/g' > perms.priv.pem.line 
    
コンフィグファイルを生成し起動します

    cd ~/mozilla-hubs-selfhost/
    ./reticulum-setup.sh
    ./reticulum-run.sh

起動すると大量にメッセージが出ます。落ち着いたところで `https://hubs.example.com:4000` （例）にアクセスします。

ポート番号は 4000 なところに注意してください。Reticulum のポート 4000 が、Mozilla Hubs をセルフホストした場合の外向けの代表ポートになります。

ログイン時には以下のユーザー登録の画面になります。

![](https://www.infiniteloop.co.jp/_sys/wp-content/uploads/2022/03/signin.png)

メールで受信した認証リンクを踏むと、以下の画面になります。

![](https://www.infiniteloop.co.jp/_sys/wp-content/uploads/2022/03/verified-640x295.png)

メール設定が上手く行ってない場合はメールでの受信が出来ないですが、その場合端末の方に流れるログメッセージにも認証リンクがありますので、そちらを使ってください。

![](https://www.infiniteloop.co.jp/_sys/wp-content/uploads/2022/03/log-640x313.png)

一人目が無事ログインまで出来たら、そのユーザーを管理者にします。端末で、リターンを何度か叩くと Elixir の iex プロンプトが出ていますので、そこで

    Ret.Account |> Ret.Repo.all() |> Enum.at(0) |> Ecto.Changeset.change(is_admin: true) |> Ret.Repo.update!()

を実行して一人目の登録したユーザーを管理者にします。(管理者不在のまま、一般ユーザーだけでもシーン作成、部屋実行、などは出来ますのでここは上手く行っていなくても心配ありません)

起動した端末はこのまま、次に新しい端末をまた開きます

### dialog サーバーの設定

最後に音声サーバーの dialog を設定、起動します。

最初に /etc/turnserver.conf 中でコメントアウトされている 'psql-userdb' の行のコメントを外して以下のように変更します

    psql-userdb="host=127.0.0.1 dbname=ret_dev user=postgres password=34jq5gaHHJF options='-c search_path=coturn' connect_timeout=30"

変更後 `systemctl restart coturn` を実行し、`systemctl status coturn` で正常に起動されているか確認します。

以下操作で設定と起動を行います。初回は C のライブラリのビルドも行うため時間がかかります。

    ./dialog-setup.sh
    ./dialog-run.sh

無事起動されていれば、ポート 4000 の Reticulum のトップページから部屋を無事作成し音声も使うことが出来るようになります。

以下、シーン作成や部屋の稼働については、シーン作成の資料を参照してください。
