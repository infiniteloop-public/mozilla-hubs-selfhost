#!/bin/bash
cd reticulum
mix deps.get
mix ecto.create
PORT=4000 MIX_ENV=prod iex -S mix phx.server
