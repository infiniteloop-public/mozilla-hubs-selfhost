#!/bin/bash
# selfhost 版 admin セットアップスクリプト

echo "selfhost 版 Mozilla Hubs セットアップ"
source ../env.sh

echo "admin のコンフィグを変更します"
sed -e "s/SELFHOST/$SELFHOST/g" admin-webpack.config.js > hubs/admin/webpack.config.js

mkdir -p hubs/admin/certs
rm hubs/admin/certs/*.pem
ln -s $HOME/fullchain.pem hubs/admin/certs/cert.pem
ln -s $HOME/privkey.pem hubs/admin/certs/key.pem

cd hubs/admin
npm ci
